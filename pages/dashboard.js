import React from "react";
import {
  Box,
} from "@mui/material";

import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";

function dashboard() {
  const theme = createTheme({
    typography: {
      fontFamily: ["Rubik"].join(","),
    },
    button: {
      fontFamily: ["Rubik"].join(","),
    },
  });

  return (
    <>
      <ThemeProvider theme={theme}>
        <Box
          sx={{
            margin: "6rem 0 0 17rem",
          }}
          display={"flex"}
          flexDirection={"row"}
        >
          <Card
            sx={{
              width: "257px",
              height: "96px",
              display: "flex",
              flexDirection: "row",
              backgroundColor: "#068BF7",
              boxShadow: "0px 12.4302px 22.948px rgba(60, 80, 224, 0.25)",
              borderRadius: "9.56168px",
              marginRight: "20px",
            }}
          >
            <CardMedia
              component="img"
              height="52.59px"
              image="/blueIcon.png"
              alt="green iguana"
              sx={{ margin: "20px", width: "51px" }}
            />
            <CardContent>
              <Box display={"flex"} flexDirection={"column"}>
                <Typography gutterBottom color={"white"}>
                  All Users
                </Typography>
                <Typography
                  color={"white"}
                  backgroundColor={"rgba(255, 255, 255, 0.2)"}
                  borderRadius={"4px"}
                  textAlign={"center"}
                >
                  246
                </Typography>
              </Box>
            </CardContent>
          </Card>
          <Card
            sx={{
              width: "257px",
              height: "96px",
              display: "flex",
              flexDirection: "row",
              backgroundColor: "#FE556F",
              boxShadow: "0px 12.4302px 22.948px rgba(60, 80, 224, 0.25)",
              borderRadius: "9.56168px",
              marginRight: "20px",
            }}
          >
            <CardMedia
              component="img"
              height="52.59px"
              image="/redIcon.png"
              alt="green iguana"
              sx={{ margin: "20px", width: "51px" }}
            />
            <CardContent>
              <Box display={"flex"} flexDirection={"column"}>
                <Typography gutterBottom color={"white"}>
                  Total Admin
                </Typography>
                <Typography
                  color={"white"}
                  backgroundColor={"rgba(255, 255, 255, 0.2)"}
                  borderRadius={"4px"}
                  textAlign={"center"}
                >
                  246
                </Typography>
              </Box>
            </CardContent>
          </Card>
          <Card
            sx={{
              width: "257px",
              height: "96px",
              display: "flex",
              flexDirection: "row",
              backgroundColor: "#F08D19",
              boxShadow: "0px 12.4302px 22.948px rgba(60, 80, 224, 0.25)",
              borderRadius: "9.56168px",
              marginRight: "20px",
            }}
          >
            <CardMedia
              component="img"
              height="52.59px"
              image="/orangeIcon.png"
              alt="green iguana"
              sx={{ margin: "20px", width: "51px" }}
            />
            <CardContent>
              <Box display={"flex"} flexDirection={"column"}>
                <Typography gutterBottom color={"white"}>
                  Total Users
                </Typography>
                <Typography
                  color={"white"}
                  backgroundColor={"rgba(255, 255, 255, 0.2)"}
                  borderRadius={"4px"}
                  textAlign={"center"}
                >
                  246
                </Typography>
              </Box>
            </CardContent>
          </Card>
        </Box>
      </ThemeProvider>
    </>
  );
}

export default dashboard;

