import * as React from "react";
import { InputBase } from "@mui/material";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Box } from "@mui/material";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Modal from "@mui/material/Modal";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const currencies = [
  {
    value: "USD",
    label: "$",
  },
  {
    value: "EUR",
    label: "€",
  },
  {
    value: "BTC",
    label: "฿",
  },
  {
    value: "JPY",
    label: "¥",
  },
];

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
};

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    backgroundColor: theme.palette.common.white,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Rahul Sharma", "rahul@gmail.com", "Admin", "Invited", ""),
  createData("Rahul Sharma", "rahul@gmail.com", "Admin", "Invited", ""),
  createData("Rahul Sharma", "rahul@gmail.com", "Admin", "Invited", ""),
  createData("Rahul Sharma", "rahul@gmail.com", "Admin", "Invited", ""),
  createData("Rahul Sharma", "rahul@gmail.com", "Admin", "Invited", ""),
  createData("Rahul Sharma", "rahul@gmail.com", "Admin", "Invited", ""),
  createData("Rahul Sharma", "rahul@gmail.com", "Admin", "Invited", ""),
];

function ChildModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Button
        onClick={handleOpen}
        sx={{
          backgroundColor: "#068BF7",
          height: "46px",
          width: "180px",
          "&:hover": {
            background: "#068BF7",
          },
        }}
      >
        <Typography
          sx={{ fontSize: "16px", color: "#FFFFFF", fontWeight: "400" }}
        >
          + New User
        </Typography>
      </Button>
      <Modal
        // hideBackdrop
        open={open}
        onClose={handleClose}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Box
          sx={{
            ...style,
            width: "417px",
            height: "611px",
            borderRadius: "6px",
          }}
        >
          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            alignItems={"center"}
          >
            <Typography sx={{ fontSize: "18px", fontWeight: "500" }}>
              Add new user
            </Typography>
            <IconButton onClick={handleClose}>
              <CloseIcon />
            </IconButton>
          </Box>
          <Typography
            sx={{
              fontSize: "16px",
              fontWeight: "400",
              color: " #808080",
              marginTop: "2rem",
            }}
          >
            User Name
          </Typography>
          <InputBase
            type="text"
            sx={{
              width: "100%",
              height: "60px",
              background: "#FFFFFF",
              border: "1px solid #E7E7E7",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
              borderRadius: "6px",
              marginTop: "5px",
              justifyItems: "center",
              padding: "20px",
            }}
          />
          <Typography
            sx={{
              fontSize: "16px",
              fontWeight: "400",
              color: " #808080",
              marginTop: "1rem",
            }}
          >
            Email ID
          </Typography>
          <InputBase
            type="email"
            sx={{
              width: "100%",
              height: "60px",
              background: "#FFFFFF",
              border: "1px solid #E7E7E7",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
              borderRadius: "6px",
              marginTop: "5px",
              justifyItems: "center",
              padding: "20px",
            }}
          />
          <Typography
            sx={{
              fontSize: "16px",
              fontWeight: "400",
              color: " #808080",
              marginTop: "1rem",
            }}
          >
            Select role
          </Typography>
          <InputBase
            type="text"
            sx={{
              width: "100%",
              height: "60px",
              background: "#FFFFFF",
              border: "1px solid #E7E7E7",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
              borderRadius: "6px",
              marginTop: "5px",
              justifyItems: "center",
              padding: "20px",
            }}
          />
          <Typography
            sx={{
              fontSize: "16px",
              fontWeight: "400",
              color: " #808080",
              marginTop: "1rem",
            }}
          >
            Enter Password
          </Typography>
          <InputBase
            type="password"
            sx={{
              width: "100%",
              height: "60px",
              background: "#FFFFFF",
              border: "1px solid #E7E7E7",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
              borderRadius: "6px",
              marginTop: "5px",
              justifyItems: "center",
              padding: "20px",
            }}
          />
          <Button
            variant="contained"
            sx={{
              width: "100%",
              height: "3rem",
              marginTop: "2rem",
              backgroundColor: "#068BF7",
            }}
          >
            Invite
          </Button>
        </Box>
      </Modal>
    </React.Fragment>
  );
}
function EditModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [currency, setCurrency] = React.useState("EUR");

  const handleChange = (event) => {
    setCurrency(event.target.value);
  };

  return (
    <React.Fragment>
      <Button
        sx={{
          width: "66px",
          height: "35px",
          backgroundColor: "#E9F5FF",
          borderRadius: "6px",
          marginRight: "10px",
        }}
        onClick={handleOpen}
      >
        <Typography
          sx={{
            textTransform: "capitalize",
            fontSize: "16px",
            fontWeight: "4000",
            color: "#068BF7",
          }}
        >
          Edit
        </Typography>
      </Button>
      <Modal
        // hideBackdrop
        open={open}
        onClose={handleClose}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Box sx={{ ...style, width: "417px", height: "493px" }}>
          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            alignItems={"center"}
          >
            <Typography sx={{ fontSize: "18px", fontWeight: "500" }}>
              Edit user
            </Typography>
            <IconButton onClick={handleClose}>
              <CloseIcon />
            </IconButton>
          </Box>
          <Typography
            sx={{
              fontSize: "16px",
              fontWeight: "400",
              color: " #808080",
              marginTop: "2rem",
            }}
          >
            User Name
          </Typography>
          <InputBase
            type="text"
            sx={{
              width: "100%",
              height: "60px",
              background: "#FFFFFF",
              border: "1px solid #E7E7E7",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
              borderRadius: "6px",
              marginTop: "5px",
              justifyItems: "center",
              padding: "20px",
            }}
          />
          <Typography
            sx={{
              fontSize: "16px",
              fontWeight: "400",
              color: " #808080",
              marginTop: "1rem",
            }}
          >
            Email ID
          </Typography>
          <InputBase
            type="email"
            sx={{
              width: "100%",
              height: "60px",
              background: "#FFFFFF",
              border: "1px solid #E7E7E7",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
              borderRadius: "6px",
              marginTop: "5px",
              justifyItems: "center",
              padding: "20px",
            }}
          />
          <Typography
            sx={{
              fontSize: "16px",
              fontWeight: "400",
              color: " #808080",
              marginTop: "1rem",
            }}
          >
            Select role
          </Typography>
          <InputBase
            type="text"
            sx={{
              width: "100%",
              height: "60px",
              background: "#FFFFFF",
              border: "1px solid #E7E7E7",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
              borderRadius: "6px",
              marginTop: "5px",
              justifyItems: "center",
              padding: "20px",
            }}
            id="outlined-select-currency"
            select
            label="Select"
            value={currency}
            onChange={handleChange}
            helperText="Please select your currency"
          >
            {currencies.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </InputBase>
          {/* <TextField
            id="outlined-select-currency"
            value={currency}
            onChange={handleChange}
            helperText="Please select your currency"
            variant="filled"
            sx={{
              width: "100%",
              height: "60px",
              background: "#FFFFFF",
              border: "1px solid #E7E7E7",
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
              borderRadius: "6px",
              marginTop: "5px",
              justifyItems: "center",
              padding: "20px",
            }}
          >
            <InputBase>
            {currencies.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
            </InputBase>
          </TextField> */}
          <Button
            variant="contained"
            sx={{
              width: "100%",
              height: "3rem",
              marginTop: "2rem",
              backgroundColor: "#068BF7",
            }}
          >
            Save
          </Button>
        </Box>
      </Modal>
    </React.Fragment>
  );
}

function userAccess() {
  const theme = createTheme({
    typography: {
      fontFamily: ["Rubik"].join(","),
    },
    button: {
      fontFamily: ["Rubik"].join(","),
    },
  });

  return (
    <>
      <ThemeProvider theme={theme}>
        <Box sx={{ margin: "6rem 0 0 17rem" }}>
          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            alignItems={"center"}
            mb={"20px"}
          >
            <Typography
              sx={{ fontSize: "18px", color: "#07003B", fontWeight: "500" }}
            >
              User Access
            </Typography>
            <ChildModal />
          </Box>
          <Box>
            <TableContainer component={Paper}>
              <Table sx={{ width: "1091px" }} aria-label="customized table">
                <TableHead>
                  <TableRow>
                    <StyledTableCell>
                      <Typography
                        sx={{
                          fontSize: "14px",
                          fontWeight: "400",
                          color: "#868686",
                        }}
                      >
                        User name
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <Typography
                        sx={{
                          fontSize: "14px",
                          fontWeight: "400",
                          color: "#868686",
                        }}
                      >
                        Email
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <Typography
                        sx={{
                          fontSize: "14px",
                          fontWeight: "400",
                          color: "#868686",
                        }}
                      >
                        Role
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <Typography
                        sx={{
                          fontSize: "14px",
                          fontWeight: "400",
                          color: "#868686",
                        }}
                      >
                        Status
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <Typography
                        sx={{
                          fontSize: "14px",
                          fontWeight: "400",
                          color: "#868686",
                        }}
                      >
                        Action
                      </Typography>
                    </StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map((row) => (
                    <StyledTableRow key={row.name}>
                      <StyledTableCell component="th" scope="row">
                        {row.name}
                      </StyledTableCell>
                      <StyledTableCell align="left">
                        {row.calories}
                      </StyledTableCell>
                      <StyledTableCell align="left">{row.fat}</StyledTableCell>
                      <StyledTableCell align="left">
                        {row.carbs}
                      </StyledTableCell>
                      <StyledTableCell align="left">
                        <EditModal />
                        <Button
                          sx={{
                            width: "66px",
                            height: "35px",
                            backgroundColor: "#FFEAED",
                            borderRadius: "6px",
                          }}
                        >
                          <Typography
                            sx={{
                              textTransform: "capitalize",
                              fontSize: "16px",
                              fontWeight: "4000",
                              color: "#FF3755",
                            }}
                          >
                            Delete
                          </Typography>
                        </Button>
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Box>
      </ThemeProvider>
    </>
  );
}

export default userAccess;
