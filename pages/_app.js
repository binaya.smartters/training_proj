import dynamic from "next/dynamic";

const DynamicSidebar = dynamic(() => import("./sidebar"), {});

function MyApp({ Component, pageProps }) {
  if (Component.getLayout) {
    return Component.getLayout(<Component {...pageProps} />);
  }
  return (
    <>
      <div style={{ display:'flex', flexDirection:'row' ,width: "100vw", height: "100vh", overflow: "hidden" }}>
        <div>
          <DynamicSidebar />
          <Component {...pageProps} />
        </div>
      </div>
    </>
  );
}

export default MyApp;
