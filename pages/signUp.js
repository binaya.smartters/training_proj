import {
  Box,
  Container,
  Typography,
  InputBase,
  Button,
  InputAdornment,
  IconButton,
} from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { VisibilityOff, Visibility } from "@mui/icons-material";
import React, { useState } from "react";
import { useRouter } from 'next/router'



function signUp() {
  const router = useRouter()

  const [values, setValues] = useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };
  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  
  const handelClick = (e) => {
    e.preventDefault();
    router.push('/dashboard');
  };

  const theme = createTheme({
    typography: {
      fontFamily: ["Nunito", "Rubik"].join(","),
    },
    button: {
      fontFamily: ["Nunito", "Rubik"].join(","),
    },
  });
  return (
    <>
      <Box display={"flex"} flexDirection={"row"}>
        <Box
          width={{ lg: "50%", md: "50%", sm: "0", xs: "0" }}
          height={"49rem"}
          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
        >
          <img src={"/1.png"} height={"100%"} width={"100%"} />
        </Box>
        <Box
          bgcolor={"white"}
          width={{ lg: "50%", md: "50%", sm: "100%", xs: "100%" }}
          height={"49rem"}
          display={"flex"}
          justifyContent={"center"}
        >
          <Container maxWidth="sm">
            <Box
              display={"flex"}
              flexDirection={"row"}
              alignItems={"center"}
              mt={"112px"}
            >
              <Box sx={{ marginRight: "10px" }}>
                <img src={"/logo.png"} />
              </Box>
              <Box>
                <ThemeProvider theme={theme}>
                  <Typography
                    variant="h2"
                    sx={{
                      letterSpacing: "0.025em",
                      color: "#1E1E1E",
                      fontWeight: "800",
                      fontSize: "38.5281px",
                    }}
                  >
                    Interview Task
                  </Typography>
                </ThemeProvider>
              </Box>
            </Box>
            <Box position={"absolute"} mt={"4rem"}>
              <ThemeProvider theme={theme}>
                <Typography
                  variant="h2"
                  sx={{
                    color: "#1E1E1E",
                    fontWeight: "500",
                    fontSize: "24px",
                    fontFamily: "Rubik",
                    color: "#068BF7",
                  }}
                >
                  Signup
                </Typography>
                <Typography
                  variant="h2"
                  mt={"5px"}
                  sx={{
                    color: "#1E1E1E",
                    fontWeight: "400",
                    fontSize: "16px",
                    fontFamily: "Rubik",
                    color: "#818181",
                  }}
                >
                  Enter your details
                </Typography>
              </ThemeProvider>
            </Box>
            <Box mt={"10rem"}>
              <Typography
                variant="h2"
                mt={"5px"}
                sx={{
                  color: "#1E1E1E",
                  fontWeight: "400",
                  fontSize: "16px",
                  fontFamily: "Rubik",
                  color: "#818181",
                }}
              >
                Enter your name
              </Typography>
              <InputBase
                type="text"
                sx={{
                  width: "100%",
                  height: "60px",
                  background: "#FFFFFF",
                  border: "1px solid #E7E7E7",
                  boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
                  borderRadius: "6px",
                  marginTop: "10px",
                  justifyItems: "center",
                  padding: "20px",
                }}
              />
            </Box>
            <Box mt={"1rem"}>
              <Typography
                variant="h2"
                mt={"5px"}
                sx={{
                  color: "#1E1E1E",
                  fontWeight: "400",
                  fontSize: "16px",
                  fontFamily: "Rubik",
                  color: "#818181",
                }}
              >
                Enter email
              </Typography>
              <InputBase
                type="email"
                sx={{
                  width: "100%",
                  height: "60px",
                  background: "#FFFFFF",
                  border: "1px solid #E7E7E7",
                  boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
                  borderRadius: "6px",
                  marginTop: "10px",
                  justifyItems: "center",
                  padding: "20px",
                }}
              />
            </Box>
            <Box mt={"1rem"}>
              <Typography
                variant="h2"
                mt={"5px"}
                sx={{
                  color: "#1E1E1E",
                  fontWeight: "400",
                  fontSize: "16px",
                  fontFamily: "Rubik",
                  color: "#818181",
                }}
              >
                Password
              </Typography>
              <InputBase
                sx={{
                  width: "100%",
                  height: "60px",
                  background: "#FFFFFF",
                  border: "1px solid #E7E7E7",
                  boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.05)",
                  borderRadius: "6px",
                  marginTop: "10px",
                  justifyItems: "center",
                  padding: "20px",
                }}
                id="standard-adornment-password"
                type={values.showPassword ? "text" : "password"}
                value={values.password}
                onChange={handleChange("password")}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {values.showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </Box>
            <Button
              variant="contained"
              mt={"2rem"}
              sx={{
                width: "100%",
                height: "60px",
                marginTop: "2rem",
                fontWeight: "400",
                fontFamily: "Rubik",
                fontSize: "16px",
                color: "#FFFFF",
              }}
              onClick={handelClick}
            >
              Continue
            </Button>
          </Container>
        </Box>
      </Box>
    </>
  );
}

export default signUp;

signUp.getLayout = function PageLayout(page){
    return(
      <>
      {page}
      </>
    )
  };