import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DashboardCustomizeIcon from "@mui/icons-material/DashboardCustomize";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { useRouter } from "next/router";
import Button from "@mui/material/Button";
import Avatar from "@mui/material/Avatar";
import Popover from "@mui/material/Popover";
import EditIcon from "@mui/icons-material/Edit";
import Container from "@mui/material/Container";
import Divider from "@mui/material/Divider";
import * as FaIcon from "react-icons/fi";
import * as FaIconAi from "react-icons/ai";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  boxShadow: "9px 38px 43px rgba(0, 0, 0, 0.1)",
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  boxShadow: "9px 38px 43px rgba(0, 0, 0, 0.1)",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  boxShadow: "9px 38px 43px rgba(0, 0, 0, 0.1)",
  width: drawerWidth,
  flexShrink: 0,
  //   whiteSpace: "nowrap",
  //   boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

export default function MiniDrawer() {
  const router = useRouter();
  const theme = createTheme({
    typography: {
      fontFamily: ["Rubik"].join(","),
    },
    button: {
      fontFamily: ["Rubik"].join(","),
    },
  });

  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handelDashboard = (e) => {
    e.preventDefault();
    router.push("./dashboard");
  };
  const handelUserAccess = (e) => {
    e.preventDefault();
    router.push("./userAccess");
  };

  const handelLogout = (e) => {
    e.preventDefault();
    router.push("./");
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const opens = Boolean(anchorEl);
  const id = opens ? "simple-popover" : undefined;

  return (
    <ThemeProvider theme={theme}>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar
          position="fixed"
          open={open}
          sx={{
            backgroundColor: "white",
            boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.05)",
          }}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              sx={{
                marginRight: 5,
                ...(open && { display: "none" }),
              }}
            >
              <Box>
                <img src={"/SidebarIcon.png"} height={"100%"} width={"100%"} />
              </Box>
            </IconButton>

            <Button
              aria-describedby={id}
              variant="contained"
              sx={{
                margin: open ? "0 0 0 68rem" : "0 0 0 79rem",
                height: "35px",
                width: "132px",
                backgroundColor: "#F3F3F3",
                color: "black",
                "&:hover": {
                  background: "#F3F3F3",
                },
              }}
              onClick={handleClick}
            >
              User Name
            </Button>
            <Avatar
              src="/profileImg.png"
              alt="Cindy Baker"
              sx={{
                marginLeft: "-1rem",
                cursor: "pointer",
                height: "46px",
                width: "3rem",
              }}
              onClick={handleClick}
            />
            <Popover
              id={id}
              open={opens}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              // sx ={{height:'20px',width:'30px'}}
            >
              <Box
                sx={{
                  height: "424px",
                  width: "417px",
                  boxShadow: "0px 8px 71px -14px rgba(0, 0, 0, 0.2)",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    // justifyContent:'center',
                    alignItems: "center",
                  }}
                >
                  <img src={"/coverImg.png"} />
                  <Box>
                    <Avatar
                      alt="Remy Sharp"
                      src="/profileImg.png"
                      sx={{
                        width: "99px",
                        height: "99px",
                        marginTop: "-3rem",
                        cursor: "pointer",
                      }}
                    ></Avatar>
                    <IconButton
                      sx={{
                        background: "#F1F1F1",
                        width: "2rem",
                        height: "2rem",
                        margin: "-4rem 0 0 4rem;",
                        "&:hover": {
                          background: "#F1F1F1",
                        },
                      }}
                    >
                      <FaIcon.FiEdit2 />
                    </IconButton>
                  </Box>
                </Box>
                <Container>
                  <Box>
                    <Typography
                      sx={{
                        color: "#068BF7",
                        fontSize: "16px",
                        fontWeight: "400",
                        marginTop: "1rem",
                        marginBottom: "5px",
                      }}
                    >
                      Profile
                    </Typography>
                    <Typography
                      sx={{
                        color: "#868686",
                        fontSize: "14px",
                        fontWeight: "400",
                        marginBottom: "5px",
                      }}
                    >
                      User name
                    </Typography>
                    <Box
                      display={"flex"}
                      flexDirection={"row"}
                      sx={{
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <Typography
                        sx={{
                          color: "#373737",
                          fontSize: "16px",
                          fontWeight: "400",
                          marginBottom: "5px",
                        }}
                      >
                        Rahul Sharma
                      </Typography>
                      <IconButton
                        sx={{
                          cursor: "pointer",
                          width: "2rem",
                          "&:hover": {
                            background: "white",
                          },
                        }}
                      >
                        <FaIcon.FiEdit2 />
                      </IconButton>
                    </Box>
                    <Divider />
                    <Typography
                      sx={{
                        color: "#868686",
                        fontSize: "14px",
                        fontWeight: "400",
                        marginBottom: "5px",
                        marginTop: "5px",
                      }}
                    >
                      Email
                    </Typography>
                    <Box
                      display={"flex"}
                      flexDirection={"row"}
                      sx={{
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <Typography
                        sx={{
                          color: "#373737",
                          fontSize: "16px",
                          fontWeight: "400",
                          marginBottom: "5px",
                        }}
                      >
                        rahul@gmail.com
                      </Typography>
                      <IconButton
                        sx={{
                          cursor: "pointer",
                          width: "2rem",
                          "&:hover": {
                            background: "white",
                          },
                        }}
                      >
                        <FaIcon.FiEdit2 />
                      </IconButton>
                    </Box>
                    <Divider />
                    <Box
                      display={"flex"}
                      flexDirection={"row"}
                      sx={{
                        justifyContent: "space-between",
                        alignItems: "center",
                        marginTop: "10px",
                      }}
                    >
                      <IconButton
                        sx={{
                          cursor: "pointer",
                          width: "2rem",
                          "&:hover": {
                            background: "white",
                          },
                        }}
                        onClick={handelLogout}
                      >
                        <FaIcon.FiLogOut />
                      </IconButton>

                      <Typography
                        sx={{
                          color: "#373737",
                          fontSize: "16px",
                          fontWeight: "400",
                          marginLeft: "-16rem",
                          cursor: "pointer",
                        }}
                        onClick={handelLogout}
                      >
                        Logout
                      </Typography>
                      <IconButton
                        sx={{
                          cursor: "pointer",
                          width: "2rem",
                          "&:hover": {
                            background: "white",
                          },
                        }}
                        onClick={handelLogout}
                      >
                        <FaIconAi.AiOutlineRight />
                      </IconButton>
                    </Box>
                  </Box>
                </Container>
              </Box>
            </Popover>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"center"}
            alignItems={"center"}
          >
            <DrawerHeader>
              <IconButton onClick={handleDrawerClose}>
                <img src={"/SidebarIcon.png"} height={"100%"} width={"100%"} />
              </IconButton>

              <Box
                sx={{ margin: "7px 10px 0 10px", cursor: "pointer" }}
                onClick={handelDashboard}
              >
                <img src={"/logo.png"} height="18.88pxs" width="18.88pxs" />
              </Box>
              <Box>
                <Typography
                  onClick={handelDashboard}
                  sx={{ cursor: "pointer" }}
                >
                  Interview Task
                </Typography>
              </Box>
            </DrawerHeader>
          </Box>
          <Box
            sx={{ margin: open ? "2rem" : "10px" }}
            display={"flex"}
            flexDirection={"column"}
          >
            <Button
              variant="contained"
              sx={{
                minWidth: "40px",
                width: open ? "176px" : "0",
                height: open ? "36px" : "36px",
                backgroundColor: "#068BF7",
              }}
              onClick={handelDashboard}
            >
              <DashboardCustomizeIcon
                sx={{
                  marginLeft: open ? -1 : 12,
                  marginRight: open ? "10px" : 0,
                  justifyContent: "center",
                  color: "white",
                }}
              />
              <Typography sx={{ opacity: open ? 1 : 0 }}>Dashboard</Typography>
            </Button>
            <Button
              sx={{
                minWidth: "40px",
                width: open ? "176px" : "10px",
                height: open ? "42px" : "42px",
                marginTop: "1rem",
                backgroundColor: "white",

              }}
              onClick={handelUserAccess}
            >
              <AccountCircleIcon
                sx={{
                  marginLeft: open ? -1 : 8,
                  marginRight: open ? "10px" : 0,
                  justifyContent: "center",
                  color: "black",
                }}
              />
              <Typography sx={{ opacity: open ? 1 : 0, color:  'black'}}>
                User Access
              </Typography>
            </Button>
          </Box>
        </Drawer>
      </Box>
      //{" "}
    </ThemeProvider>
  );
}
